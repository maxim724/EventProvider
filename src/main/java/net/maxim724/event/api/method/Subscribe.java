package net.maxim724.event.api.method;

import net.maxim724.event.api.Priority;

import java.lang.annotation.*;

/**
 * <b>Аннотация подписи</b>
 * <p>Показывает, что данный метод должен быть подписан на какое-либо событие.</p>
 *
 * @since 724.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Subscribe {

    /**
     * Получить приоритет подписчика
     * @return приоритет подписчика
     * @see Priority
     */
    Priority priority() default Priority.NORMAL;

}