package net.maxim724.event.api;

import org.jetbrains.annotations.NotNull;

/**
 * <b>Подписчик</b>
 * <p>Фунциональный интерфейс, принимающий событие данного типа и не возвращающий результат.</p>
 *
 * @param <E> тип события
 * @since 724.0
 */
@FunctionalInterface
public interface EventSubscriber<E> {

    /**
     * Вызвать подписчика на обработку события
     * @param event событие
     * @throws Throwable любое исключение во время обработки
     */
    void handle(@NotNull E event) throws Throwable;

    /**
     * Получить проиоритет подписчика
     * @return приоритет подписчика
     * @see Priority
     */
    default @NotNull Priority priority() {
        return Priority.NORMAL;
    }

}