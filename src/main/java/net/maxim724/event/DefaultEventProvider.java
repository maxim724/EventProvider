package net.maxim724.event;

import com.google.common.collect.ImmutableMap;
import net.maxim724.event.api.EventProvider;
import net.maxim724.event.api.EventSubscriber;
import net.maxim724.event.engine.SubscriberRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * <b>Провайдер событий</b>
 * <p>Простаря реализация {@link EventProvider}.</p>
 *
 * @param <E> тип события, с которым работает провайдер
 *
 * @since 724.0
 */
public class DefaultEventProvider<E> implements EventProvider<E> {
    /* Экземпляр */
    protected final Class<E> type;
    protected final SubscriberRegistry<E> registry = new SubscriberRegistry<>();

    public DefaultEventProvider(Class<E> type) {
        Objects.requireNonNull(type, "type");
        this.type = type;
    }

    @Override
    public <T extends E> void register(@NotNull Class<T> event, @NotNull EventSubscriber<? super T> subscriber) {
        /* Проверяем аргументы методов, которые будут вызваны извне */
        Objects.requireNonNull(event, "event");
        Objects.requireNonNull(subscriber, "subscriber");

        registry.register(event, subscriber);
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void publish(@NotNull E event) {
        /* Проверяем аргументы методов, которые будут вызваны извне */
        Objects.requireNonNull(event, "event");

        /* Отправляем событие на обработку подписчикам */
        for (EventSubscriber subscriber : registry.subscribers(event.getClass())) {
            try {
                subscriber.handle(event);
            } catch(Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void unregister(@NotNull EventSubscriber<?> subscriber) {
        /* Проверяем аргументы методов, которые будут вызваны извне */
        Objects.requireNonNull(subscriber, "subscriber");

        registry.unregister(subscriber);
    }

    @Override
    public void unregister(@NotNull Predicate<EventSubscriber<?>> predicate) {
        /* Проверяем аргументы методов, которые будут вызваны извне */
        registry.unregisterMatching(predicate);
    }

    @Override
    public void unregisterAll() {
        registry.unregisterAll();
    }
}